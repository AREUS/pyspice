PySpice – A Simple Macro Processor for SPICE Netlists
=====================================================

PySpice is a tool that was written to insert variable parameters into
[SPICE][] netlist templates for easy instantiation.
It is considered for use with [Ngspice][] in particular.

PySpice is part of the [AREUS][] project.

This repository can be found online on CERN's [GitLab][].

[SPICE]: https://bwrcs.eecs.berkeley.edu/Classes/IcBook/SPICE/
[Ngspice]: http://ngspice.sourceforge.net/
[AREUS]: https://gitlab.cern.ch/AREUS/AREUS
[GitLab]: https://gitlab.cern.ch/AREUS/pyspice


## Table of Contents

- [Contact](#contact)
- [Security Note](#security-note)
- [Installation](#installation)
- [Usage](#usage)
    - [Example](#example)
    - [General Usage](#general-usage)
    - [Instruction Syntax](#instruction-syntax)
    - [Variables](#variables)
- [Command Reference](#command-reference)
    - [assert](#assert)
    - [collapse dict](#collapse-dict)
    - [define](#define)
    - [define from file](#define-from-file)
    - [format](#format)
    - [insert all params](#insert-all-params)
    - [insert file](#insert-file)
    - [insert param](#insert-param)
    - [redefine](#redefine)
    - [require](#require)
    - [undefine](#undefine)


## Contact

AREUS is maintained and written by Nico Madysa (<nico.madysa@cern.ch>).


## Security Note

PySpice parses and evaluates arbitrary user-supplied Python code.
**Do not** run it on untrusted input files.


## Installation

PySpice uses [setuptools][] for installation. You can install it with
the following line.

```bash
cd pyspice
python setup.py install
```

[setuptools]: https://github.com/pypa/setuptools


## Usage

### Example

Given this file `template.py.sp`:

```spice
* THIS IS A SPICE NETLIST DEMONSTRATING THE USE OF PYSPICE *

*** pyspice: require (name='r1')                ***
*** pyspice: define (name='r2', value=9 * r1)   ***
*** pyspice: insert all params                  ***

Vs  1   0   10V
R1  1   2   {r1}
R2  2   0   {r2}

.OP
.CONTROL
    run
    print v(1,2)
    quit
.ENDC
.END
```

PySpice and NGSPICE can cooperate like this:

```bash
$ cat template.py.sp | pyspice r1=10e3 | ngspice | grep 'v(1,2)'

v(2) = 1.000000e+00
```

PySpice checks that the `r1` parameter exists and errors out if it's
missing:

```bash
$ pyspice < template.py.sp
pyspice.exceptions.RequiredVariableMissing: r1
Run again with argument -vv for a full traceback
```

### General Usage

After installation (or when inside the PySpice directory), you can run
PySpice with the following line:

```bash
pyspice
```

By default, PySpice reads from standard input and writes to standard
output. The options `--input` and `--output` can be used to read and
write other files instead.

PySpice's default mode of operation is to read the input file line by
line and to write it verbatim to the output file. However, if an input
line is recognized as a PySpice instruction, it triggers special
behavior instead.

### Instruction Syntax ###

PySpice instructions follow the following syntax:

```
*** pyspice: command (argument=value, ...) ***
```

- They must stand on a line of their own;
- they must *begin* and *end* with three asterisks (leading and
  trailing whitespace is ignored);
- the inside is the prefix `pyspice:` followed by a *command*, which
  may contain spaces;
- some commands may require additional *arguments*; these may be
  supplied in parentheses in the form `argument_name=argument_value`;
- argument values may be almost arbitrary Python expressions; some
  expressions are disabled for security reasons, however;

Note that there *must* be whitespace between these components. Do also
note that PySpice is **case-sensitive**. The following lines are not
recognized as PySpice instructions:

```
***pyspice: command***
*** pyspice:command ***
*** PYSPICE: COMMAND ***
```

The following line is recognized as an ill-formatted PySpice
instruction and causes PySpice to abort:

```
*** pyspice: command(arg=None) ***
```

### Variables

PySpice has the notion of *variables*. Variables may be defined either
on the command line or in an input file and they may be inserted into
the output file. Variables may have any name that is valid in Python
and can have any valid Python value.


## Command Reference

### assert ###

    *** pyspice: assert (cond[, message]) ***

Test whether an expression is True and aborts processing if not.

- **Args:**
    - `cond`: An expression that must evaluate to `True`.
    - `message`: An optional message that is printed if the assertion
      fails.
- **Output:** Nothing
- **Raises:**
    - `AssertionError` if `cond` evaluates to `False`.

### collapse dict ###

    *** pyspice: collapse dict (name, with_keys) ***

Recursively look up keys in a mapping until a non-mapping object is
returned or the keys are exhausted.

Essentially, `collapse dict (name='a', keys=[k1, k2, k3...])` is
equivalent to the Python code `a = a[k1][k2][k3][...]`, but it stops at
the first non-mapping object. If `a` isn't a mapping, the instruction
is a no-op.

Whether an object is a mapping is determined by whether it has a `keys`
method.

- **Args:**
    - `name`: The name of the variable that contains the mapping to
      collapse.
    - `with_keys`: The sequence of keys that shall be applied to the
      variable called `name`.
- **Output:** Nothing
- **Raises:**
    - `UndefinedVariable` if `name` doesn't name a variable.
    - `NotIterable` if `with_keys` isn't iterable.
    - `TypeError` if `with_keys` is a string.
    - `KeyError` if any look up in the variable called `name` fails.

### define ###

    *** pyspice: define (name, value) ***

Define a new variable with a given value.

- **Args:**
    - `name`: The name of the variable that gets defined.
    - `value`: The value to initialize the variable with.
- **Output:** Nothing
- **Raises:**
    - `AlreadyDefined` if a variable called `name` already exists.

### define from file ###

    *** pyspice: define from file (name, dest[, type]) ***

Read the contents of a file and interprets them as a Python object.
Then define the variable `dest` with this object as its value.

The following file formats are recognized:
- JSON (JavaScript Object Notation);
- PYON (Python Object Notation);
- YAML (Yet Another Markup Language).

YAML is only supported if the proper Python package is installed on
your system. (see [PyYAML](http://pyyaml.org/wiki/PyYAML))

- **Args:**
    - `name`: The name of the file to read from.
    - `dest`: The name of the variable to define.
    - `type`: Override the file-extension-derived file type for
      interpretation of the file contents. Valid values for `type` are
      `"json"`, `"pyon"`, and `"yaml"`.
- **Output:** Nothing
- **Raises:**
    - `IOError` if opening the specified file fails.
    - `AlreadyDefined` if the variable `dest` already exists.
    - `UnsupportedFileType` if the file type (either `type` or the file
      name suffix of `name`) is not supported.

### format ###

    *** pyspice: format (line) ***

Apply Python formatting to a string using its `format` method.

- **Args:**
    - `line`: The format string. May contain references to any defined
      variable. To output literal braces, use '{{' and '}}'
- **Output:** The formatted line.
- **Raises:**
    - `TypeError` if `line` isn't a string.
    - `UndefinedVariable` if an undefined variable is used in the
      format string.

### insert all params ###

    *** pyspice insert all params ([except]) ***

Insert one SPICE `.PARAM` line for each currently defined parameter.

- **Args:**
    - `except`: An optional iterable of variable names which to except
      from output.
- **Output:** One `.PARAM` line per parameter.
- **Raises:**
    - `NotIterable` if `except` is passed, but not iterable.
    - `InvalidName` if `except` contains any invalid variable name.
    - `UndefinedVariable` if `except` contains any name that does not
      refer to a defined variable.
    - `TypeError` if `except` is a string.

### insert file ###

    *** pyspice: insert file (name) ***

Replace this instruction contents of a given file. The inserted file is
recursively searched for PySpice instructions.

- **Args:**
    - `name`: The name of the file to be inserted.
- **Output:** The contents of the file `name`.
- **Raises:**
    - `IOError` if opening the specified file fails.

### insert param ###

    *** pyspice: insert param (name[, value]) ***

Insert a SPICE `.PARAM` line with the value of a variable.

- **Args:**
    - `name`: The variable name to insert.
    - `value`: If passed, insert this as the value for the variable. If
      not passed, the value of the variable named `name` is inserted.
- **Output:** A line of format `.PARAM <name>=<value>`.
- **Raises:**
    - `UndefinedVariable` if `name` doesn't name a variable.

### redefine ###

    *** pyspice: redefine (name, value) ***

Change the value of a previously defined variable.

- **Args:**
    - `name`: The name of a variable that was previously defined.
    - `value`: The new value of this variable.
- **Output:** Nothing
- **Raises:**
    - `UndefinedVariable` if a variable called `name` doesn't exist.

### require ###

    *** pyspice: require (name[, default]) ***

Require that a certain variable has been defined already. If it is not
defined, abort processing.

- **Args:**
    - `name`: The name of the variable that is required.
    - `default`: Optional. If passed, do not abort processing if `name`
      isn't defined. Instead, define it with the value `default`.
- **Output:** Nothing
- **Raises:**
    - `RequiredVariableMissing` if the required variable isn't defined
      *and* the `default` argument isn't supplied.

### undefine ###

    *** pyspice: undefine (name) ***

Remove a previously defined variable from PySpice memory.

- **Args:**
    - `name`: The name of the variable that gets removed.
- **Output:** Nothing
- **Raises:**
    - `UndefinedVariable` if a variable called `name` doesn't exist.
