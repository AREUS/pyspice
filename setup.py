#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

import os
import sys
import codecs
from setuptools import setup, find_packages

# Import PySpice to get its version.
here = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, here)
import pyspice

# Get the long description from the README file
with codecs.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pyspice',
    version=pyspice.__version__,
    description='Simple Macro Processor for SPICE Netlists',
    long_description=long_description,
    url='https://gitlab.cern.ch/AREUS/pyspice',
    author='Nico Madysa',
    author_email='nico.madysa@tu-dresden.de',
    license='MIT',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        ],
    keywords='cern atlas spice iktp ngspice',
    packages=find_packages(exclude=['contrib', 'docs', 'scripts', 'tests']),
    entry_points={
        'console_scripts': ['pyspice = pyspice.__main__:_main_no_args']
        },
    )
