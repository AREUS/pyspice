#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name

"""Provide additional math functions for use in PySpice instructions."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division


def totransverse(x, eta):
    """Get an observable's transverse component from its magnitude.

    Args:
        x: The observable to convert.
        eta: The pseudorapidity at which to convert.

    Returns:
        x_T: The transverse component of `x`.

    Examples:

    >>> totransverse(10.0, 0.0)
    10.0
    >>> totransverse(10.0, float('inf'))
    0.0
    >>> print(format(totransverse(10.0, 1.0), '.4f'))
    6.4805
    """
    import math
    return x / math.cosh(eta)


def fromtransverse(x_T, eta):
    """Get an observable's magnitude from its transverse component.

    Args:
        x_T: The transverse observable to convert.
        eta: The pseudorapidity at which to convert.

    Returns:
        x: The magnitude corresponding to `x_T`.

    Examples:

    >>> fromtransverse(10.0, 0.0)
    10.0
    >>> fromtransverse(0.0, float('inf'))
    nan
    >>> print(format(fromtransverse(10.0, 1.0), '.4f'))
    15.4308
    """
    import math
    return x_T * math.cosh(eta)


def _testmod():
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    _testmod()
