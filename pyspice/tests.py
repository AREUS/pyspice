#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Unit tests for PySpice."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import logging
import doctest
import unittest
import functools
from importlib import import_module
from StringIO import StringIO

import pyspice
from pyspice import exceptions


def mock_open(files):

    """Produce a function decorator that replaces `open()`.

    Args:
        files: A mapping `{name: contents}`, where `name` is the full
            path of a file and `contents` is its contents as a string.
            Opening a file in this mapping returns its contents in a
            file-like object. Opening any file that isn't in this
            mapping causes an `IOError`.

    Returns:
        A decorator `deco(func) -> func` that modifies a function in
        such a way that the built-in `open()` function is replaced by a
        mock for the duration of the function call.
    """

    def fake_file_not_found(filename):
        """Returns an `IOError`."""
        errno = os.errno.ENOENT
        return IOError(errno, os.strerror(errno), filename)

    def fake_open(name, _mode='r', _buffering=None):
        """Replacement for the built-in `open()` function."""
        try:
            file_ = StringIO(files[name])
        except KeyError:
            raise fake_file_not_found(name)
        # Spoof the file name before returning.
        file_.name = name
        return file_

    def decorator(func):
        """Replace `open()` with a fake for the duration of `func`."""
        @functools.wraps(func)
        def wrapper(*largs, **kwargs):
            """Like `func` but with a modified `open()` function."""
            old_open = __builtins__.open
            functools.update_wrapper(fake_open, old_open)
            __builtins__.open = fake_open
            try:
                return func(*largs, **kwargs)
            finally:
                __builtins__.open = old_open
        return wrapper

    return decorator


class InstructionsTestCase(unittest.TestCase):

    # pylint: disable=invalid-name

    """Base class for all test cases on PySpice instructions.

    This class implements `setUp()` and `tearDown()` for its
    descendants. The behavior is as follows:

    - setUp() calls `get_initial_state()` and assigns the result to
      `self.state` and `self.expected_state`.
    - `self.state` is used as PySpice state by the `assertProcess*()`
      methods. They use it as initial state and modify it throughout
      the unit test.
    - In the end, `self.state` and `self.expected_state` must compare
      equal. The unit test may modify `self.expected_state`
      as it sees fit.
    """

    def get_initial_state(self):
        """Return the initial dict of PySpice variables."""
        raise NotImplementedError()

    def setUp(self):
        """Set up the test."""
        self.state = self.get_initial_state()
        self.expected_state = self.get_initial_state()

    def tearDown(self):
        """Check that the variables did not change."""
        self.assertEqual(self.state, self.expected_state)

    def assertProcessSuccess(self, input_, output):
        """Assert that processing a file produces a given output.

        Args:
            input_: The string to process.
            output: The output that expected to come out of processing.
        """
        infile = StringIO(input_)
        outfile = StringIO()
        pyspice.process(infile, outfile, state=self.state)
        self.assertMultiLineEqual(output, outfile.getvalue())

    def assertProcessRaises(self, exception, input_):
        """Assert that processing a file raises an exception.

        Args:
            exception: The class of the expected exception.
            input_: The string to process.
        """
        infile = StringIO(input_)
        outfile = StringIO()
        with self.assertRaises(exception):
            pyspice.process(infile, outfile, state=self.state)


# pylint: disable=missing-docstring

class TestNoInstruction(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State()

    def test_identity(self):
        self.assertProcessSuccess("""
        A single line
        """, """
        A single line
        """)


class TestAssert(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(a=1)

    def test_success(self):
        self.assertProcessSuccess(
            """
            *** pyspice: assert (cond = a == 1) ***
            *** pyspice: assert (cond=a in {1, 2, 3}, message='no.') ***
            *** pyspice: assert (cond=True, message='Nothing') ***
            """, """
            """,
            )

    def test_fail(self):
        self.assertProcessRaises(
            AssertionError,
            """*** pyspice: assert (cond=a in {2, 3}) ***""",
            )

    def test_fail_with_message(self):
        self.assertProcessRaises(
            AssertionError,
            """*** pyspice: assert (cond=a==2, message='no') ***""",
            )

    def test_bad_call(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            """*** pyspice: assert (message='Something') ***""",
            )


class TestDefine(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State()

    def test_success(self):
        self.assertProcessSuccess(
            "*** pyspice: define (name='a', value=cos(pi)) ***",
            "",
            )
        self.expected_state = pyspice.State(a=-1.0)

    def test_missing_name(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: define (value=pi) ***",
            )

    def test_missing_value(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: define (name='a') ***",
            )

    def test_bad_name(self):
        self.assertProcessRaises(
            exceptions.InvalidName,
            "*** pyspice: define (name='0', value='b') ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: define (name=0, value='b') ***",
            )


class TestRedefine(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(a=17)

    def test_success(self):
        self.assertProcessSuccess(
            "*** pyspice: redefine (name='a', value=a+25) ***",
            "",
            )
        self.assertEqual(self.state.variables, dict(a=42))
        self.state = self.get_initial_state()

    def test_undefined_variable(self):
        self.assertProcessRaises(
            exceptions.UndefinedVariable,
            "*** pyspice: redefine (name='b', value=a+25) ***",
            )

    def test_missing_name(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: redefine (value=pi) ***",
            )

    def test_missing_value(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: redefine (name='a') ***",
            )

    def test_bad_name(self):
        self.assertProcessRaises(
            exceptions.InvalidName,
            "*** pyspice: redefine (name='a+b', value='b') ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: redefine (name=0, value='b') ***",
            )


class TestUndefine(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(a=1, b=2, c=3, d='b')

    def test_success(self):
        self.assertProcessSuccess(
            """ABC
            *** pyspice: undefine (name='a') ***
            *** pyspice: undefine (name=d) ***
            DEF""",
            """ABC
            DEF""",
            )
        self.expected_state = pyspice.State(c=3, d='b')

    def test_double_undefine(self):
        self.assertProcessRaises(
            exceptions.UndefinedVariable,
            """*** pyspice: undefine (name='a') ***
            *** pyspice: undefine (name='a') ***""",
            )
        self.expected_state = pyspice.State(b=2, c=3, d='b')

    def test_bad_name(self):
        self.assertProcessRaises(
            exceptions.InvalidName,
            "*** pyspice: undefine (name='[]') ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: undefine (name=[]) ***",
            )

    def test_no_args(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: undefine ***",
            )


class TestRequire(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State()

    def test_fail(self):
        self.assertProcessRaises(
            exceptions.RequiredVariableMissing,
            "*** pyspice: require (name='a') ***",
            )

    def test_success(self):
        self.assertProcessSuccess(
            """
            *** pyspice: define (name='a', value=cos(pi)) ***
            *** pyspice: require (name='a') ***
            *** pyspice: require (name='b', default=1) ***
            *** pyspice: require (name='b') ***
            Some text
            """,
            """
            Some text
            """,
            )
        self.expected_state = pyspice.State(a=-1.0, b=1)

    def test_bad_name(self):
        self.assertProcessRaises(
            exceptions.InvalidName,
            "*** pyspice: require (name='9gag') ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: require (name=0) ***",
            )

    def test_missing_name(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: require (default=pi) ***",
            )


class TestDefineFromFile(InstructionsTestCase):

    example = dict(a=1, b='2', c=None)

    files = {
        './a': str(example),
        './a.pyon': str(example),
        './b.txt': '{\n  "a": 1,\n  "b": "2",\n  "c": null\n  }',
        './b.json': '{\n  "a": 1,\n  "b": "2",\n  "c": null\n  }',
        './c': 'a: 1\nb: "2"\nc: null',
        './c.yaml': 'a: 1\nb: "2"\nc: null',
        './d': '\x89\x50\x4E\x47\x0D\x0A\x1A\x0A',
        }

    def get_initial_state(self):
        return pyspice.State()

    @mock_open(files)
    def test_pyon(self):
        self.assertProcessSuccess(
            "*** pyspice: define from file (name='a.pyon', dest='a') ***",
            "",
            )
        self.expected_state = pyspice.State(a=self.example)

    @mock_open(files)
    def test_json(self):
        self.assertProcessSuccess(
            "*** pyspice: define from file (name='b.json', dest='b') ***",
            "",
            )
        self.expected_state = pyspice.State(b=self.example)

    @mock_open(files)
    def test_yaml(self):
        self.assertProcessSuccess(
            "*** pyspice: define from file (name='c.yaml', dest='c') ***",
            "",
            )
        self.expected_state = pyspice.State(c=self.example)

    @mock_open(files)
    def test_explicit_pyon(self):
        self.assertProcessSuccess(
            "*** pyspice: define from file "
            "(name='a', dest='a', type='pyon') ***",
            "",
            )
        self.expected_state = pyspice.State(a=self.example)

    @mock_open(files)
    def test_explicit_json(self):
        self.state.variables['c'] = 'json'
        self.assertProcessSuccess(
            "*** pyspice: define from file "
            "(name='b.txt', dest='b', type=c) ***",
            "",
            )
        self.expected_state = pyspice.State(b=self.example, c='json')

    @mock_open(files)
    def test_explicit_yaml(self):
        self.assertProcessSuccess(
            "*** pyspice: define from file "
            "(name='c', dest='c', type='yaml') ***",
            "",
            )
        self.expected_state = pyspice.State(c=self.example)

    def test_already_defined(self):
        self.state = pyspice.State(v=1)
        self.expected_state = self.state.copy()
        self.assertProcessRaises(
            exceptions.AlreadyDefined,
            "*** pyspice: define from file (name='', dest='v') ***",
            )

    def test_missing_dest(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: define from file (name='') ***",
            )

    def test_missing_name(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: define from file (dest='') ***",
            )

    def test_bad_type_value(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: define from file (name='', dest=0, type='png') ***",
            )

    def test_bad_type_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: define from file (name='', dest=0, type=None) ***",
            )

    def test_bad_dest_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: define from file (name='', dest=0) ***",
            )

    def test_bad_file(self):
        self.assertProcessRaises(
            IOError,
            "*** pyspice: define from file (name='', dest='w') ***",
            )


class TestCollapseDict(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(
            a=dict(
                emb=dict(ps=11, front=12, middle=13, back=14),
                emec=dict(ps=21, front=22, middle=23, back=24),
                ),
            empty=dict(),
            int=42,
            )

    def test_no_collapse(self):
        self.assertProcessSuccess(
            "*** pyspice: collapse dict (name='a', with_keys=[]) ***",
            "",
            )

    def test_partial_collapse(self):
        inner = self.state.variables['a']['emb'].copy()
        self.assertProcessSuccess(
            "*** pyspice: collapse dict (name='a', with_keys=['emb']) ***",
            "",
            )
        self.expected_state.variables['a'] = inner

    def test_full_collapse(self):
        self.assertProcessSuccess(
            "*** pyspice: collapse dict "
            "    (name='a', with_keys=('emec', 'ps')) ***",
            "",
            )
        self.expected_state.variables['a'] = 21

    def test_over_collapse(self):
        self.assertProcessSuccess(
            "*** pyspice: collapse dict "
            "(name='a', with_keys=['emb', 'front', 'something']) ***",
            "",
            )
        self.expected_state.variables['a'] = 12

    def test_collapse_int(self):
        self.assertProcessSuccess(
            "*** pyspice: collapse dict (name='int', with_keys=['a']) ***",
            "",
            )

    def test_missing_key(self):
        self.assertProcessRaises(
            KeyError,
            "*** pyspice: collapse dict (name='a', with_keys=['hec']) ***",
            )

    def test_bad_keys(self):
        self.assertProcessRaises(
            exceptions.NotIterable,
            "*** pyspice: collapse dict (name='empty', with_keys=0) ***",
            )

    def test_string_key(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: collapse dict (name='empty', with_keys='key') ***",
            )

    def test_missing_keys(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: collapse dict (name='empty') ***",
            )

    def test_missing_name(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: collapse dict (with_keys=[]) ***",
            )


class TestFormat(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(a=24, b='test', c=[1, '2'])

    def test_success(self):
        self.assertProcessSuccess(
            """
            *** pyspice: format (line='test a: {a}') ***
            *** pyspice: format (line='test a: {a:.2f}') ***
            *** pyspice: format (line='test b: {b}') ***
            *** pyspice: format (line='test b: {b!r}') ***
            *** pyspice: format (line='test c: {c}') ***
            *** pyspice: format (line='test c: {{c}}') ***
            """,
            """
            test a: 24
            test a: 24.00
            test b: test
            test b: 'test'
            test c: [1, '2']
            test c: {c}
            """,
            )

    def test_undefined_variable(self):
        self.assertProcessRaises(
            exceptions.UndefinedVariable,
            "*** pyspice: format (line='{no_var}') ***",
            )

    def test_bad_format_spec(self):
        self.assertProcessRaises(
            exceptions.IndexBasedFormatting,
            "*** pyspice: format (line='{0}') ***",
            )

    def test_bad_line_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: format (line=24) ***",
            )

    def test_no_args(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: format ***",
            )


class TestInsertFile(InstructionsTestCase):

    files = {
        './a.txt': "This is an\narbitrary\ntest file.\n",
        './b.md': "no newline",
        './recurs.ive': '*** pyspice: insert file (name="recurs.ive") ***',
        }

    files_for_cwd_test = {
        './sub/a.txt':
            'Inside sub/a.txt\n'
            '*** pyspice: insert file (name="b.txt") ***\n',
        './sub/b.txt':
            'Inside sub/b.txt\n'
            '*** pyspice: insert file (name="../c.txt") ***\n',
        './sub/../c.txt':
            'Inside c.txt\n'
            '*** pyspice: insert file (name="./d.txt") ***\n',
        './sub/.././d.txt': 'Inside d.txt\n',
        }

    def get_initial_state(self):
        return pyspice.State()

    @mock_open(files)
    def test_success(self):
        self.assertProcessSuccess(
            """
            ABC
            *** pyspice: insert file (name='a.txt') ***
            DEF
            """,
            """
            ABC
            This is an
            arbitrary
            test file.
            DEF
            """
            )

    @mock_open(files)
    def test_file_without_final_newline(self):
        self.assertProcessSuccess(
            """
            ABC
            *** pyspice: insert file (name='b.md') ***
            DEF
            """,
            """
            ABC
            no newline
            DEF
            """,
            )

    @mock_open(files_for_cwd_test)
    def test_insert_changes_directory(self):
        self.assertProcessSuccess(
            """
            *** pyspice: insert file (name='sub/a.txt') ***
            """,
            """
            Inside sub/a.txt
            Inside sub/b.txt
            Inside c.txt
            Inside d.txt
            """,
            )

    @mock_open(files)
    def test_recursion_limit(self):
        self.assertProcessRaises(
            exceptions.RecursionLimitReached,
            "*** pyspice: insert file (name='recurs.ive') ***",
            )

    @mock_open(files)
    def test_bad_name(self):
        self.assertProcessRaises(
            IOError,
            "*** pyspice: insert file (name='missing') ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: insert file (name=0) ***",
            )

    def test_no_args(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: insert file ***",
            )


class TestInsertParam(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(a=1, b='125GeV', c=[1, 2, 'list'])

    def test_plain(self):
        self.assertProcessSuccess(
            """
            AB
            *** pyspice: insert param (name='a') ***
            C
            """,
            """
            AB
            .PARAM a=1
            C
            """,
            )

    def test_with_value(self):
        self.assertProcessSuccess(
            "*** pyspice: insert param (name='E_Higgs', value=b) ***",
            ".PARAM E_Higgs=125GeV\n",
            )

    def test_indirect(self):
        self.assertProcessSuccess(
            "*** pyspice: insert param (name=c[2], value=c) ***",
            ".PARAM list=[1, 2, 'list']\n",
            )

    def test_bad_name(self):
        self.assertProcessRaises(
            exceptions.InvalidName,
            "*** pyspice: insert param (name='a-') ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: insert param (name=44) ***",
            )

    def test_undefined_variable(self):
        self.assertProcessRaises(
            exceptions.UndefinedVariable,
            "*** pyspice: insert param (name='d') ***",
            )

    def test_missing_name(self):
        self.assertProcessRaises(
            exceptions.MissingArgument,
            "*** pyspice: insert param (value='d') ***",
            )


class TestInsertAllParams(InstructionsTestCase):

    def get_initial_state(self):
        return pyspice.State(a=1, b='125GeV', c=[1, 2, 'list'])

    def test_plain(self):
        self.assertProcessSuccess(
            """
            *** pyspice: insert all params ***
            """,
            """
            .PARAM a=1
            .PARAM b=125GeV
            .PARAM c=[1, 2, 'list']
            """,
            )

    def test_except(self):
        self.assertProcessSuccess(
            "*** pyspice: insert all params (except=['a', 'c']) ***",
            ".PARAM b=125GeV\n",
            )

    def test_except_all(self):
        self.assertProcessSuccess(
            "*** pyspice: insert all params (except=['a', 'b', 'c']) ***",
            "",
            )

    def test_empty(self):
        self.assertProcessSuccess(
            """
            *** pyspice: undefine (name='a') ***
            *** pyspice: undefine (name='b') ***
            *** pyspice: undefine (name='c') ***
            *** pyspice: insert all params ***
            """,
            """
            """,
            )
        self.expected_state = pyspice.State()

    def test_undefined_variable(self):
        self.assertProcessRaises(
            exceptions.UndefinedVariable,
            "*** pyspice: insert all params (except=['d']) ***",
            )

    def test_bad_name(self):
        self.assertProcessRaises(
            exceptions.InvalidName,
            "*** pyspice: insert all params (except=['23']) ***",
            )

    def test_bad_name_type(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: insert all params (except=[23]) ***",
            )

    def test_string_list(self):
        self.assertProcessRaises(
            TypeError,
            "*** pyspice: insert all params (except='a') ***",
            )

    def test_bad_list(self):
        self.assertProcessRaises(
            exceptions.NotIterable,
            "*** pyspice: insert all params (except=1) ***",
            )


def load_tests(_loader, unittests, _pattern):
    """Load both unit tests and doc tests."""
    modules = [import_module(module, __package__) for module in (
        'pyspice',
        'pyspice.__main__',
        'pyspice.exceptions',
        'pyspice.handlers',
        'pyspice.lexer',
        'pyspice.math',
        'pyspice.parser',
        'pyspice.restricted_eval',
        )]
    doctests = [doctest.DocTestSuite(module) for module in modules]
    suite = unittest.TestSuite()
    suite.addTests(unittests)
    suite.addTests(doctests)
    return suite


if __name__ == '__main__':
    # Silence the regular PySpice logger.
    logging.basicConfig(level=logging.CRITICAL+10)
    unittest.main()
