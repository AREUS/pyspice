#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Edit and execute NGSpice control files in an automated manner."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import logging
import argparse
from contextlib import contextmanager
from functools import partial
from StringIO import StringIO

def fix_sys_path():
    """If we are running from a wheel, add the wheel to `sys.path`.

    Blatantly stolen from `pip`.
    """
    if not __package__:
        # `__file__` is `<prefix>/pyspice/__main__.py`. Strip off the
        # second part to retrieve `<prefix>` and add it to the path so
        # that pyspice can be found.
        path = os.path.join(os.path.dirname(__file__), os.pardir)
        sys.path.insert(0, os.path.normpath(path))
fix_sys_path()
import pyspice  # pylint: disable=wrong-import-position
del fix_sys_path


def custom_excepthook(type_, value, traceback):
    """This function is an override for `sys.excepthook`.

    It ignores the traceback when printing exception information UNLESS
    the error level of `logging`'s root logger is at `DEBUG` or lower.
    """
    is_tb_enabled = logging.root.isEnabledFor(logging.DEBUG)
    sys.__excepthook__(type_, value, traceback if is_tb_enabled else None)
    if not is_tb_enabled:
        print('Run again with argument -vv for a full traceback',
              file=sys.stderr)


@contextmanager
def buffered_output(name, opener=None):
    """Return a context manager that buffers output to `name`.

    When entered, this context manager returns a string buffer that can
    be written to. When exited normally, the file called `name` is
    created and the buffer's contents are written to it.

    When exited through an exception, this context manager discards the
    buffer.

    Args:
        name: The name of the file.
        opener: Optional. A function that takes `name` and returns a
            writable file object. The default is `open(..., 'w')`.

    Returns:
        A context manager.
    """
    outbuf = StringIO()
    yield outbuf
    with opener(name) if opener else open(name, 'w') as outfile:
        outfile.write(outbuf.getvalue())


def read_deffile(infile):
    """Initialize the `variables` dictionary.

    The file named by `infile` is expected to only contain variable
    definitions, empty lines, and comment lines. A comment line is any
    line that starts with a hash characer '#' (and optional leading
    whitespace).

    Args:
        infile: An iterable of lines (e.g. a file object) to read
            variable definitions from.

    Returns:
        An iterator over strings containing variable definitions.
    """
    for line in infile:
        line = line.lstrip()
        if line and not line.startswith('#'):
            yield line


def format_version():
    """Return the output of `pyspice --version`."""
    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    return ' '.join(('%(prog)s', pyspice.__version__, 'from', path))


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    description, _, epilog = __doc__.partition('\n')
    parser = argparse.ArgumentParser(
        prog=pyspice.__name__,
        usage='%(prog)s [OPTION]... [NAME=VALUE]...',
        description=description,
        epilog=epilog,
        )
    parser.add_argument('-V', '--version', action='version', help='Show '
                        'version and exit.', version=format_version())
    parser.add_argument('definitions', nargs='*', metavar='NAME=VALUE',
                        help='Define variables for usage during processing.')
    parser.add_argument('--unbuffered', action='store_true',
                        help='Write the output as it is produced; the '
                        'default is to buffer the output and only write '
                        'it when processing finished without an error.')
    vgroup = parser.add_argument_group('logging control')
    vgroup.add_argument('-v', '--verbose', action='count', default=0,
                        help='Increase verbosity.')
    vgroup.add_argument('-q', '--quiet', action='count', default=0,
                        help='Decrease verbosity.')
    fgroup = parser.add_argument_group('input/output control')
    fgroup.add_argument('-i', '--input', metavar='FILE', default='-',
                        help='Input file; if "-" or not passed, read '
                        'from stdin.')
    fgroup.add_argument('-o', '--output', metavar='FILE', default='-',
                        help='Output file; if "-" or not passed, write '
                        'to stdout.')
    fgroup.add_argument('-f', '--define', metavar='FILE',
                        help='File containing additional variable '
                        'definitions, each on a separate line; pass "-" '
                        'to read from stdin. (--input cannot be "-" then)')
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)

    # Initialize logging.
    logging.basicConfig(level=logging.WARN + 10*(args.quiet-args.verbose))
    logging.info('logging level: %s', logging.getLevelName(logging.root.level))
    sys.excepthook = custom_excepthook

    # Define functions to open input/output files. In buffered mode, we
    # wrap the output file and only write it upon successful exit.
    open_read = argparse.FileType('r')
    open_write = argparse.FileType('w')
    if not args.unbuffered:
        open_write = partial(buffered_output, opener=open_write)

    # Parse input variable definitions.
    if args.define == args.input == '-':
        logging.critical('input and define file are both stdin')
        return 1
    state = pyspice.State()
    if args.define is not None:
        with open_read(args.define) as deffile:
            state.add_vardefs(read_deffile(deffile))
    state.add_vardefs(args.definitions)

    # Open input/output files and start processing.
    if args.input == args.output != '-':
        # This check avoids truncating the input file before reading it.
        logging.critical('input and output file are the same: %r', args.input)
        return 1
    with open_read(args.input) as infile:
        with open_write(args.output) as outfile:
            with state.handle_file(infile.name):
                pyspice.process(infile, outfile, state=state)
    return 0


def _main_no_args():
    """Same as calling `sys.exit(main(sys.argv[1:]))`."""
    sys.exit(main(sys.argv[1:]))


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
