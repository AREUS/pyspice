#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Functions surrounding tokenization of Python-like code."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import token
import tokenize

from pyspice import exceptions


def is_name(name):
    """Return `True` if `name` is a valid name.

    A name is valid if:
    - it's a string,
    - it consists only of alphanumeric characters and the underscore,
    - it doesn't begin with a digit, and
    - it doesn't begin with a double underscore.

    Examples:

        >>> assert is_name('name')
        >>> assert is_name('something')
        >>> assert is_name('while')
        >>> assert is_name('import')
        >>> assert is_name('do_9')

        >>> assert not is_name('lambda')
        >>> assert not is_name('__private')
        >>> assert not is_name('9something')
        >>> assert not is_name('yadda yadda')
        >>> assert not is_name('1.3')
        >>> assert not is_name('a+b')

        >>> is_name(0)
        Traceback (most recent call last):
            ...
        TypeError: not a string: 0
    """
    if not isinstance(name, str):
        raise TypeError('not a string: {0!r}'.format(name))
    # `name` must pass the blacklist.
    if _is_forbidden(name):
        return False
    # `name` must parse as a NAME token.
    tokens = iter_tokens(name)
    tok_type, tok_val = next(tokens)[:2]
    if tok_type != token.NAME or tok_val != name:
        return False
    # There mustn't be anything else following
    if next(tokens)[0] != token.ENDMARKER:
        return False
    return True


def iter_tokens(text):
    """Return an iterator over the tokens in `text`.

    Examples:
        >>> from token import tok_name
        >>> def list_tokens(text):
        ...     return [(tok_name[t[0]], t[1]) for t in iter_tokens(text)]
        >>> list_tokens('23')
        [('NUMBER', '23'), ('ENDMARKER', '')]
        >>> list_tokens('()')
        [('OP', '('), ('OP', ')'), ('ENDMARKER', '')]
        >>> list_tokens('"a", "b"')
        [('STRING', '"a"'), ('OP', ','), ('STRING', '"b"'), ('ENDMARKER', '')]
        >>> list_tokens('')
        [('ENDMARKER', '')]
    """
    iterator = iter([text])
    return tokenize.generate_tokens(lambda: next(iterator))


def iter_arguments(tokens):
    """Split (key=expr, key=expr, ...) into key-expr pairs.

    Args:
        tokens: A token stream generated from a string of the form
            "(name=expression[, name=expression, ...])", where `name` is
            an arbitrary Python identifier and `expression` is a
            restricted Python expression. (cf. `_next_expression`)

    Returns:
        A generator that yields tuples of strings `(name, expr)`.

    Raises:
        BadToken if any tokenization error occurs.
        TokenError if any unclosed open paren occurs.
        UnexpectedEol if the iterator is exhausted prematurely.

    Valid examples:

        >>> dict(iter_arguments(iter_tokens('(key="value")')))
        {'key': '"value"'}
        >>> dict(iter_arguments(iter_tokens('(l=[1, 2, 3])')))
        {'l': '[1 ,2 ,3 ]'}
        >>> dict(iter_arguments(iter_tokens('(pi=acos(-1),)')))
        {'pi': 'acos (-1 )'}
        >>> list(iter_arguments(iter_tokens('(a=1, b=2, c=3)')))
        [('a', '1 '), ('b', '2 '), ('c', '3 ')]
        >>> list(iter_arguments(iter_tokens('(a=(), b=(1,), c=(1, 2), d=3)')))
        [('a', '()'), ('b', '(1 ,)'), ('c', '(1 ,2 )'), ('d', '3 ')]

    Invalid examples raising `BadToken`:

        >>> list(iter_arguments(iter_tokens('(a=(), b=(1,), c=(1, 2)), d=3)')))
        Traceback (most recent call last):
            ...
        BadToken: expected ENDMARKER, found OP ','

        >>> list(iter_arguments(iter_tokens('key="value")')))
        Traceback (most recent call last):
            ...
        BadToken: expected OP '(', found NAME 'key'

        >>> list(iter_arguments(iter_tokens('(a="string')))
        Traceback (most recent call last):
            ...
        BadToken: expected OP, NUMBER, STRING, or NAME token, found \
ERRORTOKEN '"'

    Invalid examples raising other exceptions:

        >>> list(iter_arguments(iter_tokens('(a=(), b=(1,), c=(1, 2, d=3)')))
        Traceback (most recent call last):
            ...
        TokenError: ('EOF in multi-line statement', (2, 0))

        >>> list(iter_arguments(iter_tokens('(key="value"')))
        Traceback (most recent call last):
            ...
        TokenError: ('EOF in multi-line statement', (2, 0))

        >>> list(iter_arguments(iter_tokens('(key="value", next_k')))
        Traceback (most recent call last):
            ...
        TokenError: ('EOF in multi-line statement', (2, 0))

        >>> list(iter_arguments(iter_tokens('(a=\"\"\"string')))
        Traceback (most recent call last):
            ...
        TokenError: ('EOF in multi-line string', (1, 3))

        >>> tokens = [(token.OP, '('), (token.NAME, 'a')]
        >>> it = iter_arguments(iter(tokens))
        >>> list(it)
        Traceback (most recent call last):
            ...
        UnexpectedEol
    """
    _expect_token(tokens, Token(token.OP, '('))
    try:
        while True:
            # Parse 'name=expr' pair or final ')'.
            tok = _next_token(tokens, allowed_types=[token.NAME, token.OP])
            if tok.type == token.NAME:
                _expect_token(tokens, Token(token.OP, '='))
                name = tok.value
                expr, separator = _next_expression(tokens)
                # Interpret parsing results.
                yield name, expr
                # Break if we hit the final ')'
                if separator == ')':
                    break
            elif tok == Token(token.OP, ')'):
                break
            else:
                raise exceptions.BadToken("expected NAME or ')', found {0!s}"
                                          .format(tok))
    except StopIteration:
        raise exceptions.UnexpectedEol()
    # Make sure the token generator is exhausted. If it isn't, it means
    # we got a superfluous closing paren ')' somewhere.
    _expect_token(tokens, Token(token.ENDMARKER, ''))


def _next_expression(tokens):
    """Retrieve the next full Python expression from a stream of tokens.

    This is a helper function for `iter_arguments`. It only accepts a
    restricted subset of legal Python expressions. Forbidden are:
        - the name 'lambda';
        - any name that starts with a double underscore '__'.

    Args:
        tokens: An iterable as produced by `tokenize.generate_tokens`.

    Returns:
        expr: The next Python expression in the token stream as a string.
        separator: The separator that ended the expression. Either ','
            or ')'.

    Raises:
        BadToken if tokenization fails for any reason.

    Valid examples:

        >>> it = iter_tokens('123, 1e-12, "string", (), 1+2, a_name, '
        ...                   '[a, b], [({1:2, 3: 4}, 5), (6,), 7])')
        >>> _next_expression(it)
        ('123 ', ',')
        >>> _next_expression(it)
        ('1e-12 ', ',')
        >>> _next_expression(it)
        ('"string"', ',')
        >>> _next_expression(it)
        ('()', ',')
        >>> _next_expression(it)
        ('1 +2 ', ',')
        >>> _next_expression(it)
        ('a_name ', ',')
        >>> _next_expression(it)
        ('[a ,b ]', ',')
        >>> _next_expression(it)
        ('[({1 :2 ,3 :4 },5 ),(6 ,),7 ]', ')')

    Examples for invalid input:

        >>> _next_expression(iter_tokens('(],'))
        Traceback (most recent call last):
            ...
        BadToken: mismatched parens: '(' and ']'
        >>> _next_expression(iter_tokens(':. ...,'))
        (':....', ',')
        >>> _next_expression(iter_tokens('[a=2],'))
        ('[a =2 ]', ',')

        >>> _next_expression(iter_tokens('lambda x="bad": "input",'))
        Traceback (most recent call last):
            ...
        BadToken: illegal NAME 'lambda'
        >>> _next_expression(iter_tokens('().__class__.__base__'))
        Traceback (most recent call last):
            ...
        BadToken: illegal NAME '__class__'
    """
    def _next_allowed_token():
        return _next_token(
            tokens,
            allowed_types=[token.OP, token.NUMBER, token.STRING, token.NAME],
            )
    def _check_parens_match(left, right):
        if not {'(': ')', '[': ']', '{': '}'}[left] == right:
            raise exceptions.BadToken('mismatched parens: {0!r} and {1!r}'
                                      .format(left, right))
    def _is_opening_paren(tok):
        return tok.type == token.OP and tok.value in '([{'
    def _is_closing_paren(tok):
        return tok.type == token.OP and tok.value in ')]}'
    def _is_expr_separator(tok):
        return tok.type == token.OP and tok.value in ',)'
    def _is_illegal_name(tok):
        return tok.type == token.NAME and _is_forbidden(tok.value)
    paren_stack = []
    out_tokens = []
    while True:
        tok = _next_allowed_token()
        # Handle many special cases. Parentheses are checking for
        # matchedness, we stop parsing at a top-level ',' or ')' and we
        # suppress usage of any construct that may break the safety of
        # `restricted_eval`.
        if not paren_stack and _is_expr_separator(tok):
            separator = tok
            break
        elif _is_opening_paren(tok):
            paren_stack.append(tok.value)
        elif _is_closing_paren(tok):
            _check_parens_match(paren_stack.pop(), tok.value)
        elif _is_illegal_name(tok):
            raise exceptions.BadToken("illegal {0!s}".format(tok))
        out_tokens.append(tok)
    if not out_tokens:
        raise exceptions.BadToken('unexpected {0!s} (missing argument?)'
                                  .format(separator))
    return tokenize.untokenize(out_tokens), separator.value


class Token(object):

    # pylint: disable=too-few-public-methods

    """Type used by `_next_token()` and `_expect_token()`."""

    def __init__(self, type_, value):
        """Create an instance."""
        self.type = type_
        self.value = value

    @property
    def name(self):
        """The string representation of `self.type`."""
        return token.tok_name[self.type]

    def __iter__(self):
        """Conversion to tuple."""
        yield self.type
        yield self.value

    def __len__(self):
        """Conversion to tuple."""
        return 2

    def __getitem__(self, index):
        """Conversion to tuple."""
        # pylint: disable=consider-using-ternary
        return (self.type, self.value)[index]

    def __eq__(self, other):
        return self.type == other.type and self.value == other.value

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        if self.value:
            return '{0!s} {1!r}'.format(self.name, self.value)
        return self.name

    def __repr__(self):
        return '{0!s}({1!r}, {2!r})'.format(type(self).__name__, self.type,
                                            self.value)


def _next_token(tokens, allowed_types=(), allowed_values=''):
    """Pull the next token, optionally only accepting certain types.

    Args:
        tokens: An iterable as produced by `tokenize.generate_tokens`.
        allowed_types: A container of token types that are to be accepted.
            If not passed, any type is accepted. Token types are defined
            in the `token` standard library module.
        allowed_values: A container of values to be accepted. This allows
            e.g. to accept only a closing paren. If not passed, any
            value is accepted.

    Returns:
        tok_type: The actual type of the next token.
        value: The actual value of the next token.

    Raises:
        BadToken if the token type or value does not match the
            expectation.

    Examples:

        >>> from token import NUMBER, OP, STRING
        >>> tokens = iter_tokens('(23,)')
        >>> _next_token(tokens, [OP], '(')
        Token(51, '(')
        >>> _next_token(tokens, allowed_values=['23'])
        Token(2, '23')
        >>> # Token value does not match.
        >>> _next_token(tokens, allowed_values='()')
        Traceback (most recent call last):
            ...
        BadToken: expected '(' or ')', found ','
        >>> # Token type does not match.
        >>> _next_token(tokens, [NUMBER, STRING])
        Traceback (most recent call last):
            ...
        BadToken: expected NUMBER or STRING token, found OP ')'
    """
    tok_type, value = next(tokens)[:2]
    next_token = Token(tok_type, value)
    if allowed_types and next_token.type not in allowed_types:
        allowed_types = [token.tok_name[type_] for type_ in allowed_types]
        raise exceptions.BadToken('expected {0!s} token, found {1!s}'
                                  .format(_verbal_list(allowed_types),
                                          next_token))
    if allowed_values and next_token.value not in allowed_values:
        allowed_values = [repr(value) for value in allowed_values]
        raise exceptions.BadToken('expected {0!s}, found {1!r}'
                                  .format(_verbal_list(allowed_values),
                                          next_token.value))
    return next_token


def _expect_token(tokens, expected):
    """Pull the next token and expect a specific value and type.

    Args:
        tokens: An iterable as produced by `tokenize.generate_tokens`.
        exp_type: The expected token type. Token types are defined in
            the `token` standard library module.
        exp_value: The expected value of the token.

    Raises:
        BadToken if the token type or value does not match the
            expectation.

    Examples:

        >>> from token import NUMBER, OP
        >>> tokens = iter_tokens('(23,)')
        >>> _expect_token(tokens, Token(OP, '('))
        >>> _expect_token(tokens, Token(NUMBER, '23'))

        >>> _expect_token(tokens, Token(OP, ')'))
        Traceback (most recent call last):
            ...
        BadToken: expected OP ')', found OP ','

        >>> _expect_token(tokens, Token(NUMBER, ')'))
        Traceback (most recent call last):
            ...
        BadToken: expected NUMBER ')', found OP ')'
    """
    tok_type, value = next(tokens)[:2]
    found = Token(tok_type, value)
    if found != expected:
        raise exceptions.BadToken('expected {0!s}, found {1!s}'
                                  .format(expected, found))


def _verbal_list(words):
    """Format an iterable of words as a nice "or" list.

    Examples:

        >>> print(_verbal_list('ABC'))
        A, B, or C
        >>> print(_verbal_list(map(repr, 'ABC')))
        'A', 'B', or 'C'
        >>> print(_verbal_list('AB'))
        A or B
        >>> print(_verbal_list('A'))
        A
        >>> print(_verbal_list(''))
        <BLANKLINE>
    """
    if not words:
        return ''
    elif len(words) == 1:
        return words[0]
    elif len(words) == 2:
        return ' or '.join(words)
    head, tail = words[:-1], words[-1]
    head = ', '.join(head)
    return ', or '.join((head, tail))


def _is_forbidden(word):
    """Forbid `lambda` and leading double underscore."""
    return word == 'lambda' or word.startswith('__')


def _testmod():
    """Run doctests."""
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    _testmod()
