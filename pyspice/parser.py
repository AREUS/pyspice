#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Contains class Instruction, necessary for reading PySpice templates."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import re

from pyspice import exceptions, lexer


class Instruction(object):

    """Class that represents PySpice instructions.

    PySpice instructions may be embedded in NGSpice input files.
    NGSpice treats them as comments for compatibility.

    PySpice instruction lines are technically case-insensitive (but
    check the following note) and look as follows:

        *** pyspice: command ***
        *** pyspice: a second command ***
        *** pyspice: command with arguments (a=1, b=2) ***
        *** pyspice: cmd (all='Python', literals=['are', 'allowed']) ***

    For the exact syntax, refer to the internal functions
    `_split_instruction_line` and `_parse_arguments`.

    Note: While the instruction introduction "pyspice:" is matched
    case-insensitively, PySpice is case-sensitive in all other regards:

        >>> Instruction.from_line('*** PYSPICE: YELL (A="LOT") ***')
        Instruction('YELL', {'A': '"LOT"'}, 0)
    """

    def __init__(self, command, arguments=None, indent=0):
        """Initialize an instance.

        Args:
            command: The command part of the instruction.
            arguments: An optional dictionary that contains the
                arguments of the command as key-value pairs. The keys
                are valid Python identifiers, the values are strings
                which may be evaluated.
            indent: Optional count by how many spaces the instruction
                was originally indented. This may or may not be used
                by instruction handlers.

        Warning: It is unsafe to evaluate untrusted Python code!
        """
        self.command = command
        self.arguments = arguments
        self.indent = indent

    def __str__(self):
        """Convert the `Instruction into an instruction line.

        Examples:

            >>> print(Instruction('do the thing'))
            *** pyspice: do the thing ***
            >>> print(Instruction('do', {'the': 'thing'}, 4))
                *** pyspice: do (the='thing') ***
            >>> print(Instruction('do', {'a': 1, 'b': [1, 2, 3]}))
            *** pyspice: do (a=1, b=[1, 2, 3]) ***
        """
        if not self.arguments:
            arguments = ''
        else:
            arguments = ' ({0})'.format(
                ', '.join(
                    '{0!s}={1!r}'.format(key, value)
                    for key, value in self.arguments.items()
                    )
                )
        return '{0}*** pyspice: {1}{2} ***'.format(self.indent*' ',
                                                   self.command,
                                                   arguments)

    def __repr__(self):
        return '{0}({1!r}, {2!r}, {3:d})'.format(type(self).__name__,
                                                 self.command,
                                                 self.arguments,
                                                 self.indent)

    @staticmethod
    def in_line(line):
        """Check if a line is an instruction to PySpice.

        PySpice instructions are marked by the following rules:
            1.  they are prefixed by the word 'pyspice:', but separated
                from it by whitespace;
            2.  the pefixed text is surrounded by triple asterisks '***'
                which, again, are separated from the prefixed text by
                whitespace;
            3.  the thus-surrounded text must be on a separate line.

        Examples for valid instructions:

            >>> assert Instruction.in_line('*** pyspice: read ***')
            >>> assert Instruction.in_line(' *** pyspice: write ***\\n')
            >>> assert Instruction.in_line('\\t***\\tpyspice:\\tlock\\t***')
            >>> assert Instruction.in_line('*** pyspice: unlock (p=True) ***')
            >>> assert Instruction.in_line('*** PySpIcE: CaPsLoCk ***')
        """
        return _is_pyspice_instruction(line)

    @staticmethod
    def not_in_line(line):
        """This is equivalent to `not Instruction.in_line(line)`.

        Examples for invalid instructions:

            >>> # Wrong whitespace.
            >>> assert Instruction.not_in_line('*** pyspice:read ***')
            >>> assert Instruction.not_in_line('***pyspice: read***')
            >>> # Wrong number of asterisks.
            >>> assert Instruction.not_in_line('** pyspice: read **')
            >>> assert Instruction.not_in_line('**** pyspice: read ****')
            >>> assert Instruction.not_in_line('*** pyspice: read')
            >>> # Not a single line.
            >>> assert Instruction.not_in_line('*** pyspice:\\nread ***')
            >>> # Not on a separate line.
            >>> assert Instruction.not_in_line('; *** pyspice: read ***')
            >>> assert Instruction.not_in_line('*** pyspice: read *** ;')
        """
        return not _is_pyspice_instruction(line)

    @classmethod
    def from_line(cls, line):
        """Parse an instruction line and return the an `Instruction` object.

        Args:
            line: The instruction line to be parsed. See the method
                `Instruction.in_line` for the definition.

        Returns:
            The resulting `Instruction` object.

        Raises:
            See `_split_instruction_line` and `_parse_arguments` for
            a list of possible exceptions.

        Examples:

            >>> Instruction.from_line('*** pyspice: do the thing ***')
            Instruction('do the thing', None, 0)
            >>> Instruction.from_line('*** pyspice: do (the="thing") ***')
            Instruction('do', {'the': '"thing"'}, 0)
            >>> Instruction.from_line('***pyspice: do the thing***')
            Traceback (most recent call last):
                ...
            NotAnInstructionLine: ***pyspice: do the thing***
        """
        command, arguments, indent = _split_instruction_line(line)
        if arguments is not None:
            arguments = _parse_arguments(arguments)
        return cls(command, arguments, indent)


def _split_instruction_line(line):
    """Extract command and arguments part of an instruction line.

    Args:
        line: The instruction line to be parsed. See the method
            `Instruction.in_line` for the definition.

    Returns:
        command: The lowercase command part of the instruction line.
        arguments: The string containing the arguments of the
            command if there are any; `None` otherwise.
        indent: The number of spaces that the line was indented by.
            Tab characters '\t' count as 8 spaces.

    Raises:
        NotAnInstructionLine if the line doesn't match the regular
            expression for instruction lines.

    Valid examples:

        >>> _split_instruction_line('\\t*** pyspice: do the thing ***')
        ('do the thing', None, 8)
        >>> _split_instruction_line('  *** pyspice: do (the="thing") ***')
        ('do', '(the="thing")', 2)
        >>> _split_instruction_line('*** pyspice: do (broken="thing"]]]) ***')
        ('do', '(broken="thing"]]])', 0)

    Invalid examples:

        >>> _split_instruction_line('***pyspice: do the thing***')
        Traceback (most recent call last):
            ...
        NotAnInstructionLine: ***pyspice: do the thing***
        >>> _split_instruction_line('*** pyspice: do [the="thing"] ***')
        Traceback (most recent call last):
            ...
        NotAnInstructionLine: *** pyspice: do [the="thing"] ***
    """
    match = re.match(
        r"""
        ^(\s*)          # The line may have arbitrary leading whitespace.
        \*\*\*          # Then comes the instruction marker and
        \s+             # more whitespace. Followed by the
        pyspice:        # instruction introduction and
        \s+             # more whitespace.
        (\w+(\s+\w+)*)  # The command, a series of words, is followed by
        (\s+(           # arguments, whitespace-separated from the command.
            \(.*\)      # Arguments are enclosed in parentheses.
        ))?             # Arguments are optional.
        \s+             # This is followed by more whitespace.
        \*\*\*$         # The line ends with the instruction marker.
        """,
        line,
        re.VERBOSE+re.IGNORECASE,
        )
    if match is None:
        raise exceptions.NotAnInstructionLine(line)
    leading_space, command, _, _, arguments = match.groups()
    indent = len(leading_space.expandtabs())
    return command, arguments, indent


def _parse_arguments(text):
    """Wrapper around `lexer.iter_arguments()` that returns a `dict`.

    Args:
        text: The string to parse. Must start with an open paren '(',
            and must end with a closing paren ')'.

    Returns:
        a `dict` containing the arguments.

    Raises:
        Exception of any kind that `iter_arguments` may raise.
        DuplicateArgument if two items have the same key name.

    Examples:

        >>> _parse_arguments('(key="value")')
        {'key': '"value"'}
        >>> sorted(_parse_arguments('(a=1, b=2, c=3)').items())
        [('a', '1 '), ('b', '2 '), ('c', '3 ')]

        >>> _parse_arguments('(a=1, a=2)')
        Traceback (most recent call last):
            ...
        DuplicateArgument: a
    """
    tokens = lexer.iter_tokens(text)
    arguments = {}
    for name, expr in lexer.iter_arguments(tokens):
        if name in arguments:
            raise exceptions.DuplicateArgument(name)
        arguments[name] = expr
    return arguments


def _is_pyspice_instruction(line):
    """Implementation of `Instruction.in_line`."""
    marker = '***'
    prefix = 'pyspice:'
    # Quick check to avoid splitting each and every line.
    line = line.strip()
    if not (line.startswith(marker) and line.endswith(marker)):
        return False
    elif '\n' in line:
        return False
    # Actually check for whitespace-separated words.
    words = line.split()
    return words[0] == words[-1] == marker and words[1].lower() == prefix


def _testmod():
    """Run doctests."""
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    _testmod()
