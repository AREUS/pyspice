#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""PySpice – A Simple Macro Processor for SPICE Netlists

This is a program used for templatization of SPICE input files.
Regular SPICE files are transmitted one-to-one from stdin to stdout.
However, if PySpice encounters a so-called "instruction line", it
may cause PySpice to produce special output or to change its internal
state.

See the file README.md for more information.

SPICE is a general-purpose circuit simulation program for nonlinear dc,
nonlinear transient, and linear ac analyses. SPICE originates from the
EECS Department of the University of California at Berkeley. Find out
more at https://bwrcs.eecs.berkeley.edu/Classes/IcBook/SPICE/
"""

import os
import ast
import logging
from contextlib import contextmanager

from pyspice import exceptions, handlers
from pyspice.lexer import is_name
from pyspice.parser import Instruction
from pyspice.version import version as __version__


class State(object):

    """Type of the internal state of the PySpice interpreter.

    Objects of this type are passed into `process()` and from there to
    the various instruction handlers. The handlers may read and modify
    the state.
    """

    def __init__(self, *largs, **kwargs):
        """Create an instance.

        All arguments are used to initialize the dict `self.variables`.

        Examples:

            >>> State().variables
            {}
            >>> State({1: 2}).variables
            {1: 2}
            >>> State(a='b').variables
            {'a': 'b'}
        """
        self.variables = dict(*largs, **kwargs)
        self._dirstack = []

    def __repr__(self):
        return '<{name} with variables={v} and dirstack={ds}>'.format(
            name=type(self).__name__,
            v=self.variables,
            ds=self._dirstack
            )

    def __eq__(self, other):
        # pylint: disable=protected-access
        if isinstance(other, type(self)):
            return (self.variables == other.variables
                    and self._dirstack == other._dirstack)
        return False

    def __ne__(self, other):
        return not self == other

    def copy(self):
        # pylint: disable=protected-access
        """Create a shallow copy of the state."""
        copied = State(self.variables)
        copied._dirstack = list(self._dirstack)
        return copied

    def add_vardefs(self, vardefs):
        """Convenience wrapper around `update()` and `parse_vardef()`.

        This parses all variable definitions in `vardefs` via
        `parse_vardef` and updates `self.variables` with the result.

        Args:
            vardefs: An iterable of strings like `key=value`.

        Examples:

            >>> s = State()
            >>> s.add_vardefs(['a="b"', 'c=None'])
            >>> sorted(s.variables.items())
            [('a', 'b'), ('c', None)]
        """
        self.variables.update(parse_vardef(vd) for vd in vardefs)

    @property
    def root(self):
        """The current root for relative paths."""
        return self._dirstack[-1] if self._dirstack else os.curdir

    @contextmanager
    def handle_file(self, path):
        """Start handling the contents of the file at a given path.

        Instructions may reference files by relative paths. In order to
        avoid surprises, these paths should always be relative to the
        directory of the file that is being processed.

        Args:
            path: The path of the file that will be processed.

        Returns:
            A context manager declaring that we now handle a certain
            file. The context manager modifies `self.root` when entered
            and undoes this change when exited.

        Examples:

            >>> state = State()
            >>> state.root
            '.'
            >>> with state.handle_file('somewhere/else/file.txt'):
            ...     state.root
            'somewhere/else'
            >>> state.root
            '.'
            >>> # We coincidentally handle stdin correctly:
            >>> import sys
            >>> with state.handle_file('outer/file.txt'):
            ...     print(repr(state.root))
            ...     with state.handle_file(sys.stdin.name):
            ...         print(repr(state.root))
            'outer'
            '.'
        """
        filedir = os.path.split(path)[0] or os.curdir
        self._dirstack.append(filedir)
        try:
            yield
        finally:
            popped = self._dirstack.pop()
            # Ensure that contexts don't get interleaved.
            assert popped == filedir

    def handle_expansion(self, expansion):
        """Like `handle_file()`, but more polymorphic.

        Args:
            expansion: Either a file object or another iterable of
            lines. If it is a file (has a `name` attribute), this
            returns the context manager from
            `handle_file(expansion.name)`. Otherwise, this returns a
            context manager that does nothing.
        """
        if hasattr(expansion, 'name'):
            return self.handle_file(expansion.name)
        @contextmanager
        def _null_manager():
            yield
        return _null_manager()


def process(infile, outfile, recursion_limit=10, state=None):
    """Process a file, searching and replacing PySpice instructions.

    Args:
        infile: File-like object to read from.
        outfile: File-like object to write to.
        recursion_limit: How many times this function may call itself.
        state: An instance of `State`. Contains e.g. the variables used
            by PySpice for editing the input file.

    Raises:
        RecursionException if a PySpice instruction line is encountered
            and `recursion_limit <= 0`.
    """
    if not isinstance(recursion_limit, int):
        raise TypeError('recursion_limit must be an integer')
    if state is None:
        state = State()
    logger = logging.getLogger(__name__)
    filename = getattr(infile, 'name', '<no file>')
    logger.debug('Beginning handling of file %s', filename)
    try:
        for lineno, line in enumerate(infile, 1):
            if Instruction.in_line(line):
                logger.debug('line %d: found PySpice instruction', lineno)
                logger.debug('line %d: current recursion limit is %d',
                             lineno, recursion_limit)
                if recursion_limit <= 0:
                    raise exceptions.RecursionLimitReached()
                instruction = Instruction.from_line(line)
                logger.info('line %d: %s', lineno, repr(instruction))
                expansion = handlers.handle_instruction(instruction, state)
                # Only process the expansion if it has any content.
                if expansion:
                    # `handle_expansion()` takes care of switching
                    # directories if we're processing a new file.
                    with state.handle_expansion(expansion):
                        process(expansion, outfile,
                                recursion_limit=recursion_limit-1,
                                state=state)
            else:
                outfile.write(line)
    except Exception:
        # Get an input-file-based traceback.
        logger.critical('%s: line %d: exception raised during processing',
                        filename, lineno)
        raise


def parse_vardef(vardef):
    """Parse a variable definition of the form `key=value`.

    A variable definition is a string of form 'name=literal', where
    name is a valid Python name and literal is a valid Python literal.
    Whitespace around the equal sign '=' is allowed. Quotation marks
    around a string may be dropped if the string does not start with
    any kind of opening parenthesis (round, square, or curly.) See the
    examples.

    Args:
        vardef: The variable definition to parse.

    Returns:
        name: The name of the defined variable. This must consist only
            of ASCII letters, digits, and the underscore. The first
            character must not be a digit.
        value: The value of the defined variable. This function attempts
            to parse it using `ast.literal_eval`. If this fails with a
            SyntaxError, the value is taken as a string as-is. This
            simplifies passing strings from the command-line, which
            generally strips away quotes entered by the user.

    Raises:
        InvalidName if `name` is not a valid Python name.
        SyntaxError if `value` is interpreted as a Python literal, but
            then fails to parse as such.
        ValueError if `value` is a Python literal that contains
            non-literal sub-expressions.

    Examples:

    Simple integers and floats are parsed, but no hexadecimals:

        >>> parse_vardef('a=1')
        ('a', 1)
        >>> parse_vardef('a=1.0')
        ('a', 1.0)
        >>> parse_vardef('a=0xff')
        ('a', '0xff')

    Complex Python literals can be parsed as well:

        >>> parse_vardef('a = {1: 2, 3: 4}')
        ('a', {1: 2, 3: 4})
        >>> parse_vardef('a = (0,)')
        ('a', (0,))
        >>> parse_vardef('a=[1, 2, (3, 4)]')
        ('a', [1, 2, (3, 4)])
        >>> parse_vardef(r'a=r"\\\\"')
        ('a', '\\\\\\\\')
        >>> parse_vardef('a=[1, 2,')
        Traceback (most recent call last):
            ...
        SyntaxError: unexpected EOF while parsing

    Quotes around simple strings may (but needn't) be dropped:

        >>> parse_vardef('a=1GeV')
        ('a', '1GeV')
        >>> parse_vardef('a=emb')
        ('a', 'emb')
        >>> parse_vardef('a="emb"')
        ('a', 'emb')
        >>> parse_vardef('a=\"\"\"emb\"\"\"')
        ('a', 'emb')
        >>> parse_vardef('a="\\'emb\\'"')
        ('a', "'emb'")
        >>> parse_vardef('a="emb')
        Traceback (most recent call last):
            ...
        SyntaxError: EOL while scanning string literal

    Around other strings, they are necessary.

        >>> parse_vardef('a="{1: 2}"')
        ('a', '{1: 2}')
        >>> parse_vardef('a=note(important)')
        Traceback (most recent call last):
            ...
        ValueError: malformed string in value part of 'a=note(important)'

    Nested strings must be quoted as well.

        >>> parse_vardef('a={a: 1}')
        Traceback (most recent call last):
            ...
        ValueError: malformed string in value part of 'a={a: 1}'
        >>> parse_vardef('a={"a": 1}')
        ('a', {'a': 1})

    A collection of other errors:

        >>> parse_vardef('9notaname=0')
        Traceback (most recent call last):
            ...
        InvalidName: 9notaname
        >>> parse_vardef('__builtins__=0')
        Traceback (most recent call last):
            ...
        InvalidName: __builtins__
        >>> parse_vardef('name=')
        Traceback (most recent call last):
            ...
        ValueError: missing value for variable 'name'
    """
    name, _, value = (s.strip() for s in vardef.partition('='))
    if not is_name(name):
        raise exceptions.InvalidName(name)
    # Characters to check all of `value` against. We cannot just check
    # the first character, because the string might start with nonsense
    # like `'\\\n\\\n\\\n'`.
    parens = set('([{"\'')
    if not value:
        # First, check that we have any value at all.
        raise ValueError('missing value for variable {0!r}'.format(name))
    elif parens.intersection(value) or value in ['True', 'False', 'None']:
        # Then, parse anything that looks like a non-trivial Python
        # object. This covers lists, tuples, dicts, sets, bools, `None`,
        # and all strings.
        try:
            value = ast.literal_eval(value)
        except ValueError as exc:
            (msg,) = exc.args
            msg += ' in value part of {0!r}'.format(vardef)
            exc.args = (msg,)
            raise
    elif value.isdigit():
        # If it contains only digits, it must be an integer.
        value = int(value, base=10)
    else:
        # Then only floats are left. If that fails, too, we treat it as
        # an unquoted string.
        try:
            value = float(value)
        except ValueError:
            pass
    return name, value
