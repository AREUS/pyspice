#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Provide `restricted_eval`, a quick and dirty sandbox around `eval`."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division


def _getglobals():
    """Return a dict of allowed global variables."""
    def _init():
        """Calculate the value."""
        import math
        import pyspice.math
        result = vars(math).copy()
        result.update(vars(pyspice.math))
        _builtins = (__builtins__ if isinstance(__builtins__, dict)
                     else vars(__builtins__))
        whitelist = [
            'False', 'True', 'None', 'abs', 'all', 'any', 'bin',
            'bool', 'chr', 'complex', 'cmp', 'dict', 'divmod',
            'enumerate', 'filter', 'float', 'format', 'frozenset',
            'hash', 'hex', 'int', 'isinstance', 'issubclass', 'iter',
            'len', 'list', 'long', 'map', 'max', 'min', 'next', 'oct',
            'ord', 'pow', 'range', 'reduce', 'repr', 'reversed',
            'slice', 'set', 'sorted', 'str', 'sum', 'tuple', 'type',
            'unicode', 'vars', 'zip',
            ]
        result['__builtins__'] = dict((name, _builtins[name])
                                      for name in whitelist)
        return result
    # The main logic.
    try:
        return _getglobals.result
    except AttributeError:
        _getglobals.result = _init()
        return _getglobals.result


def restricted_eval(code, variables=None):
    # pylint: disable=eval-used
    """Evaluate arbitrary Python code in a restricted environment.

    This `eval` function uses a `globals` dict in which most built-in
    functions are undefined -- most importantly the `__import__`
    function.
    However, be aware that this is *not* necessarily safe. There are
    exploits against this model, see e.g.
    http://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html

    As an additional measure, you should disallow all names with leading
    double underscore from `code`. Also consider disallowing `lambda`.

    Args:
        code: Python string or code object forwarded to `eval`.
        variables: A dict of local variables available to `eval`.

    Returns:
        The result of `eval`.

    Examples:

        >>> restricted_eval('2')
        2
        >>> restricted_eval('cos(pi)', {})
        -1.0
        >>> restricted_eval('a + b', {'a': 20, 'b': 22})
        42
        >>> restricted_eval('int(pi)')
        3
        >>> restricted_eval('totransverse(10.0, 0.0)')
        10.0
        >>> restricted_eval('__import__("os")')
        Traceback (most recent call last):
            ...
        NameError: name '__import__' is not defined

    Example of a circumvention of this function:

        >>> restricted_eval(\"\"\"[
        ...     c for c in ().__class__.__base__.__subclasses__()
        ...     if c.__name__ == 'catch_warnings'
        ...     ][0]()._module.__builtins__['compile']\"\"\")
        <built-in function compile>
    """
    variables = variables if variables is not None else {}
    return eval(code, _getglobals(), variables)
