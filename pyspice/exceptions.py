#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Exception hierarchy for PySpice."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division


class ParserError(Exception):
    """An error occurred while parsing an instruction."""


class NotAnInstructionLine(ParserError):
    """Input violates the syntax of PySpice instruction lines."""


class UnexpectedEol(ParserError):
    """Instruction line ended when PySpice was still expecting input."""


class DuplicateArgument(ParserError):
    """Two arguments to an instruction have the same key name."""


class BadToken(ParserError):
    """Unexpected token while parsing an instruction line."""


class InterpreterError(Exception):
    """An error occurred while interpreting an instruction."""


class UnknownInstruction(InterpreterError):
    """An instruction doesn't have a registered handler."""


class UndefinedVariable(InterpreterError):
    """An undefined variable is referenced in an instruction."""


class RequiredVariableMissing(InterpreterError):
    """An variable named in a `require` has not been defined."""


class AlreadyDefined(InterpreterError):
    """A variable gets defined multiple times."""


class MissingArgument(InterpreterError):
    """A required argument to an instruction is missing."""


class InvalidName(InterpreterError):
    """An invalid variable name has been passed."""


class NotIterable(InterpreterError):
    """An variable is expected to be iterable, but isn't."""


class NotAMapping(InterpreterError):
    """An variable is expected to be a mapping, but isn't."""


class IndexBasedFormatting(InterpreterError):
    """An format string refers to an argument by index."""


class UnsupportedFileType(InterpreterError):
    """An unknown `type` argument was passed to `define from file`."""


class RecursionLimitReached(Exception):
    """Recursion limit of function `process` reached."""
