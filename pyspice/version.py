#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name

"""Auto-generated module containing the PySpice version string."""

version = '1.2.0'
