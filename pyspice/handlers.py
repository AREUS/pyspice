#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Contains PySpice instruction handlers."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import ast
import json
try:
    import yaml
except ImportError:
    pass

from pyspice import exceptions
from pyspice.lexer import is_name
from pyspice.restricted_eval import restricted_eval


class Expansion(object):

    # pylint: disable=too-few-public-methods

    """Wrapper around iterables of lines that makes them consistent.

    This type wraps lists of lines, generators of lines, and file
    objects. It mimicks them to a minimal extent, in particular:

    - if the iterable has a `__len__`, so does the `Expansion`;
    - if the iterable has a `name`, so does the `Expansion`.
    - it implements `iter()` and `bool()`.

    The `Expansion` further indents all its lines by a configurable
    amount and ensures that all lines end in a newline character.
    """

    def __init__(self, iterable, indent=0):
        """Create an instance.

        Args:
            iterable: The object to be wrapped.
            indent: The amount of spaces by which all lines shall be
                indented.
        """
        self._inner = iterable
        self._lead = indent * ' '
        if hasattr(iterable, 'name'):
            self.name = iterable.name
        if hasattr(iterable, '__len__'):
            self.__len__ = iterable.__len__

    def __nonzero__(self):
        """This object is `True` if the wrapped object is."""
        return bool(self._inner)

    def __iter__(self):
        """Iterate over the indented lines."""
        for line in self._inner:
            trail = '' if line.endswith('\n') else '\n'
            yield ''.join((self._lead, line, trail))


def handle_instruction(instruction, state, _handlers={}):
    # pylint: disable=dangerous-default-value
    """Handle a PySpice instruction.

    Args:
        instruction: The result of parsing an instruction line.
        state: An instance of `pyspice.State`.

    Returns:
        An iterator over the lines that the instruction expands to.

    Raises:
        UnknownInstruction if there is no known handler for an
            instruction.
    """
    command = instruction.command
    if instruction.arguments is None:
        arguments = {}
    else:
        # Evaluate all the arguments. We know that they can't contain
        # the name `lambda` nor any name that starts with '__'. Together
        # with `restricted_eval`, this should be pretty safe.
        arguments = dict((name, restricted_eval(arg, state.variables))
                         for (name, arg) in instruction.arguments.items())
    # Look up the right construction handler.
    if not _handlers:
        _handlers.update({
            'assert': handle_assert,
            'define': handle_define,
            'redefine': handle_redefine,
            'undefine': handle_undefine,
            'require': handle_require,
            'define from file': handle_define_from_file,
            'collapse dict': handle_collapse_dict,
            'format': handle_format,
            'insert file': handle_insert_file,
            'insert param': handle_insert_param,
            'insert all params': handle_insert_all_params,
            })
    try:
        handler = _handlers[command]
    except KeyError:
        raise exceptions.UnknownInstruction(command)
    lines = handler(arguments, state)
    # If `lines` is empty, return it directly, otherwise expand it.
    return lines and Expansion(lines, indent=instruction.indent)


def handle_assert(arguments, _state):
    """Handle the `assert` command.

    The `assert` command tests whether an expression is True and aborts
    processing if not.

    Syntax:
        `assert (cond[, message])`

    Args:
        cond: An expression that must evaluate to `True`.
        message: An optional message that is printed if the assertion
            fails.

    Output:
        Nothing

    Raises:
        AssertionError if `cond` evaluates to `False`.
    """
    cond = _getarg(arguments, 'cond')
    if 'message' in arguments:
        exc_args = (_getarg(arguments, 'message'),)
    else:
        exc_args = ()
    # Do not just use `assert`, Python might eventually decide to
    # ignore it when running in optimization mode.
    if not cond:
        raise AssertionError(*exc_args)


def handle_define(arguments, state):
    """Handle the `define` command.

    The `define` command defines a new variable with a given value.

    Syntax:
        `define (name, value)`

    Args:
        name: The name of the variable that gets defined.
        value: The value to initialize the variable with.

    Output:
        Nothing

    Raises:
        AlreadyDefined if a variable called `name` already exists.
    """
    name = _getname(arguments, 'name')
    value = _getarg(arguments, 'value')
    if name in state.variables:
        raise exceptions.AlreadyDefined(name)
    state.variables[name] = value


def handle_redefine(arguments, state):
    """Handle the `redefine` command.

    The `redefine` command changes the value of a previously defined
    variable.

    Syntax:
        `redefine (name, value)`

    Args:
        name: The name of a variable that was previously defined.
        value: The new value of this variable.

    Output:
        Nothing

    Raises:
        UndefinedVariable if a variable called `name` already exists.
    """
    name = _getname(arguments, 'name')
    value = _getarg(arguments, 'value')
    if name not in state.variables:
        raise exceptions.UndefinedVariable(name)
    state.variables[name] = value


def handle_undefine(arguments, state):
    """Handle the `undefine` command.

    The `undefine` command removes a previously defined variable from
    PySpice memory.

    Syntax:
        `undefine (name)`

    Args:
        name: The name of the variable that gets removed.

    Output:
        Nothing

    Raises:
        UndefinedVariable if a variable called `name` doesn't exist.
    """
    name = _getname(arguments, 'name')
    try:
        del state.variables[name]
    except KeyError:
        raise exceptions.UndefinedVariable(name)


def handle_require(arguments, state):
    """Handle the `require` command.

    The `require` command requires that a certain variable has been
    defined already. If it is not defined, processing aborts.

    Syntax:
        `require (name[, default])`

    Args:
        name: The name of the variable that is required.
        default: Optional. If passed, do not abort processing if `name`
            isn't defined. Instead, define it with the value `default`.

    Output:
        Nothing

    Raises:
        RequiredVariableMissing if the required variable isn't defined.
    """
    name = _getname(arguments, 'name')
    if name not in state.variables:
        try:
            state.variables[name] = _getarg(arguments, 'default')
        except exceptions.MissingArgument:
            raise exceptions.RequiredVariableMissing(name)


def handle_define_from_file(arguments, state):
    """Handle the `define from file` command.

    The `define from file` command reads the contents of a file and
    interprets them as a Python object. The variable `dest` is then
    newly defined with this object as its value.

    The following file formats are recognized:
    - JSON (JavaScript Object Notation);
    - PYON (Python Object Notation);
    - YAML (Yet Another Markup Language).

    YAML is only supported if the proper Python package is installed
    on your system. (see [PyYAML](http://pyyaml.org/wiki/PyYAML))

    Syntax:
        `define from file (name, dest[, type])`

    Args:
        name: The name of the file to read from.
        dest: The name of the variable to define.
        type: Override the file-extension-derived file type for
            interpretation of the file contents.

    Output:
        Nothing

    Raises:
        IOError if opening the specified file fails.
        AlreadyDefined if the variable `dest` already exists.
        UnsupportedFileType if the file type (either `type` or the
            file name suffix of `name`) is not supported.
    """
    name = _getarg(arguments, 'name')
    dest = _getname(arguments, 'dest')
    type_ = ''
    if dest in state.variables:
        raise exceptions.AlreadyDefined(dest)
    # Handle the optional argument.
    if 'type' in arguments:
        type_ = _getarg(arguments, 'type')
    else:
        _, ext = os.path.splitext(name)
        # Remove leading period '.'.
        if ext:
            type_ = ext[1:]
    path = os.path.join(state.root, name)
    value = _read_file_contents(open(path, 'r'), type_)
    state.variables[dest] = value


def handle_collapse_dict(arguments, state):
    """Handle the `collapse dict` command.

    The `collapse dict` command recursively looks up keys in a mapping
    until a non-mapping object is returned or the keys are exhausted.
    Essentially, `collapse dict (name='a', keys=[k1, k2, k3...])` is
    equivalent to the Python code `a = a[k1][k2][k3][...]`, except that
    it stops at the first non-mapping object.

    Whether an object is a mapping is determined by whether it has a
    `keys` method.

    Syntax:
        `collapse dict (name, with_keys)`

    Args:
        name: The name of the variable that contains the mapping to
            collapse.
        with_keys: The sequence of keys that shall be applied to the
            variable called `name`.

    Output:
        Nothing

    Raises:
        UndefinedVariable if `name` doesn't name a variable.
        NotIterable if `with_keys` isn't iterable.
        TypeError if `with_keys` is a string.
        KeyError if any look up in the variable called `name` fails.
    """
    # Get the arguments.
    name = _getname(arguments, 'name')
    with_keys = _getarg(arguments, 'with_keys')
    if isinstance(with_keys, str):
        raise TypeError('must not be a string: {0!r}'.format(with_keys))
    elif not hasattr(with_keys, '__iter__'):
        raise exceptions.NotIterable(with_keys)
    # Retrieve the mapping.
    try:
        the_dict = state.variables[name]
    except KeyError:
        raise exceptions.UndefinedVariable(name)
    # Apply the keys.
    for key in with_keys:
        if not hasattr(the_dict, 'keys'):
            break
        the_dict = the_dict[key]
    state.variables[name] = the_dict


def handle_format(arguments, state):
    """Handle the `format` command.

    The `format` command applies Python formatting to a string using
    its `format` method.
    All variables defined before this command are available in the
    format string.

    Syntax:
        `format (line)`

    Args:
        line: The format string. May contain references to any defined
            variable. To output literal braces, use '{{' and '}}'

    Output:
        The formatted line.

    Raises:
        TypeError if `line` isn't a string.
        UndefinedVariable if an undefined variable is used in the format
            string.
    """
    line = _getarg(arguments, 'line')+'\n'
    try:
        line = line.format(**state.variables)
    except AttributeError:
        raise TypeError('not a string: {0!r}'.format(line))
    except KeyError as exc:
        (name,) = exc.args
        raise exceptions.UndefinedVariable(name)
    except IndexError:
        raise exceptions.IndexBasedFormatting(line)
    return [line]


def handle_insert_file(arguments, state):
    """Handle the `insert file` command.

    The `insert file` command replaces its own occurence with the
    contents of a given file.

    Syntax:
        `insert file (name)`

    Args:
        name: The name of the file to be inserted.

    Output:
        The contents of the file `name`.

    Raises:
        IOError if opening the specified file fails.
    """
    name = _getarg(arguments, 'name')
    root = state.root
    try:
        path = os.path.join(root, name)
    except Exception:
        raise TypeError('not a string: {0!r}'.format(name))
    # Return a file, not a list, so that PySpice knows we're entering
    # a different file (`state.root` will change).
    return open(path, 'r')


def handle_insert_param(arguments, state):
    """Handle the `insert file` command.

    The `insert param` command replaces its own occurence with an
    NGSpice `.PARAM` line.

    Syntax:
        `insert param (name[, value])`

    Args:
        name: The variable name to insert.
        value: If passed, insert this as the value for the variable.
            If not passed, the value of the variable named `name` is
            inserted.

    Output:
        A line of format `.PARAM <name>=<value>`.

    Raises:
        UndefinedVariable if `name` doesn't name a variable.
    """
    name = _getname(arguments, 'name')
    if 'value' in arguments:
        value = _getarg(arguments, 'value')
    else:
        try:
            value = state.variables[name]
        except KeyError:
            raise exceptions.UndefinedVariable(name)
    return ['.PARAM {0}={1}\n'.format(name, value)]


def handle_insert_all_params(arguments, state):
    """Handle the `insert all params` command.

    The `insert all params` command replaces its own occurence with an
    NGSpice `.PARAM` line for each defined parameter.

    Syntax:
        `insert all params ([except])`

    Args:
        except: An optional iterable of variable names which to except
            from output.

    Output:
        One `.PARAM` line per parameter.

    Raises:
        NotIterable if `except` is passed, but not iterable.
        InvalidName if `except` contains any invalid variable name.
        UndefinedVariable if `except` contains any name not referring
            to a defined variable.
        TypeError if `except` is a string.
    """
    if 'except' in arguments:
        ignored = _getarg(arguments, 'except')
        if isinstance(ignored, str):
            raise TypeError('must not be a string: {0!r}'.format(ignored))
        try:
            ignored = list(ignored)
        except TypeError:
            raise exceptions.NotIterable(ignored)
    else:
        ignored = []
    for name in ignored:
        if not is_name(name):
            raise exceptions.InvalidName(name)
        if not name in state.variables:
            raise exceptions.UndefinedVariable(name)
    return [
        '.PARAM {0!s}={1!s}\n'.format(name, value)
        for (name, value) in sorted(state.variables.items())
        if name not in ignored
        ]


def _read_file_contents(file_, type_):
    """Internal implementation of `handle_define_from_file`.

    Args:
        file_: The file-like object to be read.
        type: The file type to assume.

    Returns:
        The contents of `file_` parsed as a Python object using either
        the JSON decoder, the YAML decoder, or `ast.literal_eval`.

    Raises:
        IOError if opening the specified file fails.
        UnsupportedFileType if `type_` specifies none of the understood
            file types. (JSON, PYON, optionally YAML)
    """
    loaders = _read_file_contents.loaders
    if not loaders:
        loaders.update(
            json=json.load,
            pyon=(lambda stream: ast.literal_eval(stream.read())),
            yaml=yaml.load if 'yaml' in globals() else None
            )
    # Get the proper loader. `load is None` if the `yaml` package
    # couldn't be imported.
    try:
        load = loaders[type_.lower()]
    except KeyError:
        raise exceptions.UnsupportedFileType(type_)
    if load is None:
        raise exceptions.UnsupportedFileType(type_ + ' (missing dependency)')
    return load(file_)
_read_file_contents.loaders = dict()


def _getarg(arguments, name):
    """Look-up `name` in the mapping `arguments`.

    Raises:
        MissingArgument if the key `name` cannot be found.
    """
    try:
        return arguments[name]
    except KeyError:
        raise exceptions.MissingArgument(name)


def _getname(arguments, name):
    """Look-up the variable name `name` in the mapping `arguments`.

    Raises:
        MissingArgument if the key `name` cannot be found.
        InvalidName if the value behind `name` isn't a valid name.
    """
    name = _getarg(arguments, name)
    if not is_name(name):
        raise exceptions.InvalidName(name)
    return name


def _testmod():
    """Run doctests."""
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    _testmod()
