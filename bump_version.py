#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Make a Git commit that bumps PySpice's version."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import re
import sys
import argparse
from subprocess import check_output, check_call

from pkg_resources import parse_version


VERSION_FILE_TEMPLATE = u"""\
#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# pylint: disable=invalid-name

\"\"\"Auto-generated module containing the PySpice version string.\"\"\"

version = {version!r}
"""


def check_version_string(version):
    """Fail if the version is not formatted like `MAJOR.MINOR.PATCH.`"""
    match = re.match(r'^\d+\.\d+\.\d+$', str(version))
    if not match:
        raise ValueError('bad version string: '+str(version))


def compare_version_to_git(version):
    """Fail if the latest git tag is a newer version than the given one."""
    describe = check_output(['git', 'describe', '--always', '--tags'])
    git_version = parse_version(describe.partition('-')[0])
    if version <= git_version:
        raise ValueError('expected version newer that {0!s}, got {1!s}'
                         .format(git_version, version))


def check_git_status():
    """Check the working directory for uncommited changes."""
    status = check_output(['git', 'status', '--porcelain']).rstrip()
    if status:
        raise RuntimeError('working directory is unclean according to '
                           'git\n'+status)


def write_version_file(name, version):
    """Generate version.py."""
    with open(name, 'w') as outfile:
        outfile.write(VERSION_FILE_TEMPLATE.format(version=str(version)))


def make_tagged_commit(name, version):
    """Commit the file `name` and tag the commit as `version`."""
    message = 'Bump version to {0!s}.'.format(version)
    check_call(['git', 'commit', name, '--message', message])
    check_call(['git', 'tag', str(version)])


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('version', metavar='MAJOR.MINOR.PATCH',
                        type=parse_version, help='the new version')
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    check_version_string(args.version)
    compare_version_to_git(args.version)
    check_git_status()
    filename = 'pyspice/version.py'
    write_version_file(filename, args.version)
    make_tagged_commit(filename, args.version)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
